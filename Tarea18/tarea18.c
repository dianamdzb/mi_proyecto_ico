#include <stdio.h>
#include "cajero.h"
int main(int argc, char const *argv[]) {
  int saldo=0;
  int opcion=1;
  while (opcion) {
    opcion=menu();
  switch (opcion) {
    case 1:
      depositar();
      break;
    case 2:
      retirar();
      break;
    case 3:
      donar();
      break;
    case 0:
      printf("Saliendo\n");
      break;
    default:
      printf("opcion no valida\n");
  }
}
  return 0;
}
