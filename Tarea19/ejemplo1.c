#include <stdio.h>
int main(int argc, char const *argv[]) {
  char arreglo[6];
  arreglo[0]='A';
  arreglo[1]='B';
  arreglo[2]='C';
  arreglo[3]='D';
  arreglo[4]='E';
  arreglo[5]='F';

  printf("El valor del subindice 0 es : %c",arreglo[0]);
  printf("\nEl valor del subindice 1 es : %c",arreglo[1]);
  printf("\nEl valor del subindice 2 es : %c",arreglo[2]);
  printf("\nEl valor del subindice 3 es : %c",arreglo[3]);
  printf("\nEl valor del subindice 4 es : %c",arreglo[4]);
  printf("\nEl valor del subindice 5 es : %c\n",arreglo[5]);
  return 0;
}
